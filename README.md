# けもけもメイドさんTRPG

## はじめに

そこは平和な世界。大きなお屋敷があって、それは本当に大きなお屋敷で。メイド達がたくさんいても、全部掃除しきれるか不安になるほど、大きな大きなお屋敷。

お屋敷には優しいご主人様がいます。ご主人様はいつもメイド達に優しくて、頼んだことをきちんとやれば、頭を撫で撫でしてくれます。そうすると、メイド達はもう嬉しくて仕方がなくなってしまうのです。

さあ、今日もご主人様がメイド達を呼んでいます。どうやらお仕事ができたようです。どのメイドも、耳や尻尾をぴくぴくとさせながら、喜んで走って行くのでした……そう、このお屋敷のメイド達は、みな、動物のような耳や尻尾、角を持っている「けもけもメイド」なのでした

## 真実

```
あなたは本来は普通の人間だったが何らかの理由によりけもけもメイドの姿になってしまった。
もし元の記憶を保っていることを知られたら、今度こそ記憶を奪われてしまうだろう。

この館の秘密を探りつつ
メイドとして疑われないように
ご主人様の機嫌を取ってより自由に動けるようになりつつ

...元に戻るための調査をするのだ
```

## キャラクターメイク

### 能力値

初期値はすべて1で、そこに6点自由に配分します。

| なまえ | せつめい |
| ---- | ---- |
| おしごと | 掃除にお料理、子守になくし物発見！メイドさんはいろいろできます |
| にちじょう | 普段の暮らしでも難しいことってありますよね？得意なものだってあるのです |
| ひみつ| メイドのお仕事には無関係の、どうして使えるのかわからないふしぎな能力 |

### かわいい/もとのきおく

#### かわいい

    「かわいい」はどれだけ「けもメイド」として可愛らしくなっているか
    を示します。
    「かわいい」ポイントの回数分、「かわいい」判定を行うことができます
    初期値は1です

#### もとのきおく

    「もとのきおく」はもともとの「人間としての姿の時の記憶」を示します。
    「かわいい」が増えるとき「もとのきおく」は減っていき、
    0になるとロストしNPCメイドとして再登場します。
    初期値は5になります。

### 種族

| D6 | 種族名 | けもスキル |
| -- | -- |  -- |
| 1 |いぬ | 「超嗅覚」匂いを覚え、どこまでも追跡できる |
| 2 |ねこ | スキルは「忍び足」気配や足音を立てずに移動可能 |
| 3 |とり | 「空を飛ぶ」腕を翼にかえて空を飛ぶ |
| 4 |ひつじ | 「癒やし」ふわふわ空間を作り出し、敵対心を喪わせたり癒やし空間にしたりする | 
| 5 |きつね |「霊感」この世ならざるものと会話したり、呼び出したり|
| 6 |うさぎ | 「超跳躍」普通ではジャンプできないものでも軽々と |
| - |うし | 「突進」頭からどーん！相手は吹っ飛ぶ。|
| - |くま | 「怪力」力任せにどんなことでも解決だ！何事も力で解決！ |

### 外見・雰囲気

キャラクターの外見・雰囲気を決定します。自由に決めてもよいですし、下表をそれぞれ2回判定して決めても構いません。
結果が同じだった場合、あるいはお互いに矛盾するような内容の場合は好きに決めるか、再判定してください。

#### 雰囲気表

| D66 | 雰囲気 |
| --  | -- |
| 11 | 凛々しい |
| 12 | 冷徹 |
| 13 | 知的 |
| 14 | 寡黙 |
| 15 | 策士 |
| 16 | 老獪 |
| 22 | かわいい |
| 23 | おさない |
| 24 | ぼうっとしてる |
| 25 | 甘えん坊 |
| 26 | まもってあげたい |
| 33 | 勇敢 |
| 34 | かっこいい |
| 35 | 善良 |
| 36 | 元気 |
| 44 | きれい |
| 45 | 妖艶 |
| 46 | みだらな |
| 55 | 純情 |
| 56 | ただものではない |
| 66 | 自由に決定 |

#### 外見表

| D66 | 外見 |
| --  | -- |
| 11 | オッドアイ |
| 12 | 時々後光がさす |
| 13 | 髪が虹色 |
| 14 | もふもふ |
| 15 | 超ロングヘア |
| 16 | オーラが黒い |
| 22 | 巨乳 |
| 23 | 貧乳 |
| 24 | 幼児 |
| 25 | 豊満 |
| 26 | モデル |
| 33 | 髪が素敵 |
| 34 | 目が素敵 |
| 35 | (けも部分)が素敵 |
| 36 | 唇が素敵 |
| 44 | 眼鏡をかけた |
| 45 | 眼帯をかけた |
| 46 | リボンをつけた |
| 55 | 生傷が絶えない |
| 56 | 頬に傷がある |
| 66 | 自由に決定 |


### スキル

| 分類 | スキル名 |
| --- | --- |
| おしごと | 掃除 裁縫 料理 子守 修繕 力仕事 礼儀 教養 博識 農業 酪農 園芸 |
| にちじょう | 走る 投げる s泳ぐ 隠す 隠れる 探す 釣り パソコン 歌う おしゃれ 踊る 読書|
| ひみつ | 開錠 尾行 闇討 狙撃 回避 毒薬 恫喝 密造 詐術 解体 潜入 |

## 判定

### スキル判定

- スキルがある場合

能力値+2D6

- スキルがない場合

1D6

### 「かわいい」判定

スキル判定に失敗したとき「かわいい」ポイントがあれば "能力値+3D6" で判定を再度試みることができます。かわいければ大抵のことはどうにかなります。

ただし、この判定に成功した場合は1D6を振ってください。その目が「かわいい」を下回った場合は、即座に「かわいい」が+1され、「もとのきおく」が-1されます

# シナリオ

## 構成

1日は「朝」「昼」「夜」「深夜」のシーンで構成されます。シナリオの長さにおうじて、全部で何日かかるかを決めてください。

## 流れ

シナリオの最初に「大きなお仕事」をGMは提示します。シナリオ内で解決すればよい課題です。

そして、1日の最初に「小さなお仕事」をGMはプレイヤーに提示してください。小さなお仕事を解決するために他のプレイヤーの協力が必要な場合は、そのプレイヤーが参加を拒まなければ共同で行動することも可能です。そのシーンで解決した時、協力したプレイヤー全員が「達成点」と「ご褒美」を得られます。

## 1日の行動の制限

1日のうち1回は「休息」に費やさなければ行けません。眠らなかった場合、翌日のすべての判定にペナルティが加わります。ペナルティは「徹夜状態で過ごしているシーン数」x2になります。このペナルティの結果、判定値が0以下になった場合、キャラクターは眠ってしまいます。

## ご褒美

ご褒美は以下から選択してください

- ご主人様の寵愛

    ご主人様に寵愛されます。「かわいい」が+1され、「もとのきおく」判定に失敗すると「もとのきおく」が1減少します。また、寵愛により「自由時間」と「ご主人様におねだり」のいずれかを得られます。

- 自由時間

    翌日「小さなお仕事」から解放されます。翌日のみ、事件が解決しなくても「達成点」を得ることができます。

- ご主人様におねだり

    ご主人様におねだりし「他のPCの情報」「ご主人様しか入れない部屋の鍵」「ご主人様しか知らない情報」を得ることができます。

## 各シーンでのプレイヤーの行動

各プレイヤーは自分のシーンで以下の行動を選択できます

- 大きなお仕事をする
- 小さなお仕事をする
- 秘密を探る
- 休息する
- お部屋を探索する
- 何もしない(パス)

## 大きなお仕事

小さなお仕事で得た達成点と、各シーンでの「大きなお仕事の達成点」を足して目標値に到達した時点で大きなお仕事の達成になります。

大きなお仕事を達成するとゲーム終了になります。この時点で「元の姿に戻る」手がかりを得ている場合、そのキャラクターは「元に戻る」か「メイドのままでいる」かを判定することになります。


```
例)
大きなお仕事 : 倉庫のお片づけ！(期限 : 3日、必要達成度10)
1日目・小さなお仕事 : 
お庭の掃除(必要達成度 : 3、必要時間 : 1行動ぶん
)
1日目・小さなお仕事 : 
お庭の掃除(必要達成度 : 3、最低必要時間 : 1行動ぶん)
2日目・小さなお仕事 : 
ご主人様のお手伝い(必要達成度 : 4、最低必要時間 : 1行動分)
3日目・小さなお仕事(必要達成度 : 6、最低必要時間 : 2行動ぶん)
```